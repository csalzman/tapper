﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public float rotationSpeed;

	void FixedUpdate () {
		gameObject.transform.Rotate(new Vector3(0,0,rotationSpeed));
	}
}
