﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (SpriteRenderer))]
public class ColorSwapScript : MonoBehaviour {

	public ColorOptions currentColor = ColorOptions.Dark;

	//GameControllerScript
	GameControllerScript gameController;

	//Find the Sprite
	SpriteRenderer sprite;

	void Start() {
		gameController = GameObject.Find("GameController").GetComponent<GameControllerScript>();

		//find the sprite on the object
		sprite = gameObject.GetComponent<SpriteRenderer>();
		if(currentColor == ColorOptions.Light) {
			sprite.color = gameController.light;
		}
		else {
			sprite.color = gameController.dark;
		}
	}

	public void swapItSwapItGood() {
		if(currentColor == ColorOptions.Light) {
			currentColor = ColorOptions.Dark;
			sprite.color = gameController.dark;
		}
		else {
			currentColor = ColorOptions.Light;
			sprite.color = gameController.light;

		}
	}
}
