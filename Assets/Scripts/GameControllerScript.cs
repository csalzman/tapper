﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public enum ColorOptions {
	Light,
	Dark,
	Missed
}

public class GameControllerScript : MonoBehaviour {

	public GameObject littleCirclePrefab;

	[Header("Colors")]
	public Color light;
	public Color dark;
	public Color missed;

	[Header("How Often to Spawn Circles")]
	public float newCircleTimer;

	public bool pause = false;

	void Start() {
		//Setup
		Camera.main.backgroundColor = dark;
		StartCoroutine(countDown());
	}

	public Text countDownText;

	public IEnumerator countDown() {
		pause = true;
		countDownText.text = "3";
		yield return new WaitForSeconds(1.0f);
		countDownText.text = "2";
		yield return new WaitForSeconds(1.0f);
		countDownText.text = "1";
		yield return new WaitForSeconds(1.0f);
		countDownText.text = "Go";
		pause = false;
		yield return new WaitForSeconds(1.0f);
		countDownText.text = "";
		yield return false;
	}

	public GameObject optionsCanvas;

	public void showOptions() {
		Instantiate(optionsCanvas);
	}

	void Update () {
		if(!pause) {
			if(Time.time - newCircleTimer >1f) {
				GameObject lilCircle = Instantiate(littleCirclePrefab, gameObject.transform.position, Quaternion.identity) as GameObject;

				if(Random.value < .5f) {
					lilCircle.GetComponent<LittleCircleScript>().setColor(ColorOptions.Light);
				}
				else {
					lilCircle.GetComponent<LittleCircleScript>().setColor(ColorOptions.Dark);
				}
				newCircleTimer = Time.time;
			}

			//Change everything when the button is hit
			if(Input.GetKeyDown(KeyCode.A)) {
				Debug.Log("down");
				swapColors();
				StartCoroutine(fadeBackground(dark, light, 11f));
			}
			if(Input.GetKeyUp(KeyCode.A)) {
				Debug.Log("up");
				swapColors();
				StartCoroutine(fadeBackground(light, dark, 11f));
			}
		}

		if (Input.GetKeyDown(KeyCode.Q)) {
			Application.Quit();
		}

		if (!pause && Input.GetKeyDown(KeyCode.P)) {
			if(pause) {
				pause = false;
			}
			else {
				pause = true;

				//Tell allthe little circles to stop 
				LittleCircleScript[] lilCircles = GameObject.FindObjectsOfType<LittleCircleScript>();
				foreach(LittleCircleScript lilCircle in lilCircles) {
					lilCircle.pauseDestroy();
				}
			}
		}
	}

	//Finds all of the objects that can be swaps and tells them to cha-cha-change themselves
	public void swapColors() {
		ColorSwapScript[] colorswaps = GameObject.FindObjectsOfType<ColorSwapScript>();
		foreach(ColorSwapScript swap in colorswaps) {
			swap.swapItSwapItGood();
		}
	}

	public IEnumerator fadeBackground(Color colorStart, Color colorEnd, float speedModifier) {
		//When does all this start?
		float movementStart = Time.time;

		while(Camera.main.backgroundColor != colorEnd) {
			Camera.main.backgroundColor = Color.Lerp(colorStart, colorEnd, (Time.time - movementStart) * speedModifier);
			yield return true;
		}
		yield return null;
	}
}
