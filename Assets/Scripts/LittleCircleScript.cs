﻿using UnityEngine;
using System.Collections;

public enum Direction {
	Up,
	Down,
	Left,
	Right,
	UpLeft,
	UpRight,
	DownLeft,
	DownRight
}
public class LittleCircleScript : MonoBehaviour {

	[Header("Current Color")]
	public ColorOptions currentColor;

	[Header("Distance from center")]
	public float distanceFromCenter = 5f;

	[Header("Speed Range")]
	public float slowSpeed = .1f;
	public float fastSpeed = .5f;

	Direction startingDirection;

	//GameControllerScript
	[HideInInspector] public GameControllerScript gameController;

	//Used to set the color of the circle
	public void setColor(ColorOptions newColor) {
		currentColor = newColor;
		switch(newColor) {
		case ColorOptions.Light:
			gameObject.GetComponent<SpriteRenderer>().color = gameController.light;
			break;
		case ColorOptions.Dark:
			gameObject.GetComponent<SpriteRenderer>().color = gameController.dark;
			break;
		case ColorOptions.Missed:
			gameObject.GetComponent<SpriteRenderer>().color = gameController.missed;
			break;
		}
	}

	void Awake() {
		gameController = GameObject.Find("GameController").GetComponent<GameControllerScript>();
	}

	// Use this for initialization
	void Start () {

		startingDirection = (Direction)Random.Range(0, 8);	

		switch(startingDirection) {
		case Direction.Up:
			gameObject.transform.position = new Vector3(0,distanceFromCenter,1f);
			break;
		case Direction.Down:
			gameObject.transform.position = new Vector3(0,-distanceFromCenter,1f);
			break;
		case Direction.Left:
			gameObject.transform.position = new Vector3(-distanceFromCenter,0f,1f);
			break;
		case Direction.Right:
			gameObject.transform.position = new Vector3(distanceFromCenter,0f,1f);
			break;
		case Direction.UpLeft:
			gameObject.transform.position = new Vector3(distanceFromCenter,-distanceFromCenter,1f);
			break;
		case Direction.UpRight:
			gameObject.transform.position = new Vector3(distanceFromCenter,distanceFromCenter,1f);
			break;
		case Direction.DownLeft:
			gameObject.transform.position = new Vector3(-distanceFromCenter,-distanceFromCenter,1f);
			break;
		case Direction.DownRight:
			gameObject.transform.position = new Vector3(-distanceFromCenter,distanceFromCenter,1f);
			break;
		default:
			gameObject.transform.position = new Vector3(5f,0f,1f);
			break;
		}

		StartCoroutine(moveCircle(gameObject.transform.position, new Vector2(0,0), Random.Range(slowSpeed, fastSpeed)));
	}

	public IEnumerator moveCircle(Vector3 startPos, Vector3 endPos, float speedModifier) {
		//Current Position should start at start position and will slowly move towards end position
		Vector3 currentPos = gameObject.transform.position;

		//When does all this start?
		float movementStart = Time.time;

		//Move her from point A to point B in increments of 1 pixel
		while(currentPos != endPos) {
			//Recheck this every frame
			currentPos = gameObject.transform.position;

			Vector2 updatedPos = Vector2.Lerp (startPos, endPos, (Time.time - movementStart) * speedModifier);

			gameObject.transform.position = updatedPos;

			yield return null;
		}

		if(currentPos == endPos) {
			StartCoroutine(bomb(gameObject.transform.position, 1f));
		}
	}
		
	public bool colorCheck(ColorOptions colorOfPulse) {
		Debug.Log("checked");
		if(currentColor == colorOfPulse) {
			return true;
		}
		else {
			return false;
		}
	}

	public void pauseDestroy() {
		Destroy(gameObject);
	}

	public IEnumerator bomb(Vector3 startPos, float speedModifier) {

		//Change Color
		setColor(ColorOptions.Missed);

		//Current Position should start at start position and will slowly move towards end position
		Vector3 currentPos = gameObject.transform.position;

		//When does all this start?
		float movementStart = Time.time;


		float vertical;
		//Randomly make some of them black
		if(Random.value < .5f) {
			vertical = 1000;
		}
		else{
			vertical = -1000;
		}

		Vector3 endPos = new Vector3(Random.Range(-1000,1000),vertical,1);

		//Move her from point A to point B in increments of 1 pixel
		while(currentPos != endPos) {
			//Recheck this every frame
			currentPos = gameObject.transform.position;

			Vector2 updatedPos = Vector2.Lerp (startPos, endPos, (Time.time - movementStart) * speedModifier);

			gameObject.transform.position = updatedPos;

			yield return null;
		}
	}
}
