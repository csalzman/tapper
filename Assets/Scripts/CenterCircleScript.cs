﻿using UnityEngine;
using System.Collections;

public class CenterCircleScript : MonoBehaviour {

	[HideInInspector] public GameControllerScript gameController;

	void Awake() {
		gameController = GameObject.Find("GameController").GetComponent<GameControllerScript>();
	}

	public IEnumerator pulsate() {
		gameObject.transform.localScale += new Vector3(.1f,.1f,1f);
		yield return new WaitForSeconds(.1f);
		gameObject.transform.localScale -= new Vector3(.1f,.1f,1f);
		yield return null;
	}

	void OnTriggerStay2D(Collider2D coll) {
		//On the Object that has entered, check if the color is the same as the color of the center circle
		//If it is the same:
		if(coll.gameObject.GetComponent<LittleCircleScript>().colorCheck(gameObject.GetComponent<ColorSwapScript>().currentColor)) {
			StartCoroutine(pulsate());
			Destroy(coll.gameObject);
		}
		//If it isn't
		else {
			
		}
	}
}
