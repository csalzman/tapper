﻿using UnityEngine;
using System.Collections;

public class LevelBuildingScript : MonoBehaviour {

	[System.Serializable]
	public class animationOptions {
		public bool slowRotate;

		public enum rotationDirection {
			Left,
			Right
		}

		public bool moveTopBottom;

		public rotationDirection rotateDirection;
	}

	[Header("Animation Options")]
	public animationOptions options;

	[HideInInspector] public GameControllerScript gameController;

	void Start() {
		gameController = GameObject.Find("GameController").GetComponent<GameControllerScript>();
	}

	// Update is called once per frame
	void Update () {
		if(!gameController.pause) {
			if(options.slowRotate) {
				if(options.rotateDirection == animationOptions.rotationDirection.Left) {
					gameObject.transform.Rotate(new Vector3(0,0,-1));
				}
				else {
					gameObject.transform.Rotate(new Vector3(0,0,1));
				}
			}

			if(options.moveTopBottom) {
				transform.position = new Vector3(transform.position.x, Mathf.PingPong(Time.time, 3)*100, transform.position.z);
			}
		}
	}
}
